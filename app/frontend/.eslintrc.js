module.exports = {
  env: {
    node: true,
  },

  extends: ['plugin:vue/essential', 'prettier'],

  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },

  parserOptions: {
    parser: '@typescript-eslint/parser',
  },

  plugins: ['vue'],

  rules: {
    indent: ['warn', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: [1, 'single'],
    semi: ['error', 'always'],
  },

  extends: ['plugin:vue/essential', 'prettier', '@vue/typescript'],
};
