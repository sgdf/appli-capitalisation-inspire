import axios, { AxiosResponse, AxiosError } from 'axios';
import { authStore, JwtData } from '@/store/auth';
import axiosClient from './axiosClient';

export const loginUser = async (
  username: string,
  password: string,
): Promise<
  AxiosResponse<JwtData> | AxiosError<{ non_field_errors: unknown }>
> => {
  let response;
  try {
    response = await axiosClient.post('/auth/jwt/create/', {
      username,
      password,
    });
  } catch (error) {
    return Promise.reject(error);
  }
  authStore.login(response.data);
  return Promise.resolve(response);
};
