export const createDebounce = () => {
  let timeout: number | undefined = undefined;
  return function (func: () => void, delayMs?: number) {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func();
    }, delayMs || 500);
  };
};
