export const isForumAspirationEnabled = (): boolean =>
  process.env?.VUE_APP_ALLOW_FORUM_ASPIRATIONS === 'enabled';

export const isRegistrationEnabled = (): boolean =>
  process.env?.VUE_APP_ALLOW_REGISTRATION === 'enabled';

export const isPriorisationEnabled = (): boolean =>
  process.env?.VUE_APP_ALLOW_PRIORISATION === 'enabled';
