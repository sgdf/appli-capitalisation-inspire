import { authStore } from '@/store/auth';
import axios, { AxiosRequestConfig } from 'axios';

const API_URL = process.env?.VUE_APP_API_URL;

if (!API_URL) {
  throw new Error('Environment variable VUE_APP_API_URL is not defined!');
}

const axiosConfig: AxiosRequestConfig = { baseURL: API_URL };

const axiosClient = axios.create(axiosConfig);

/** Axios client with JWT handling */
axiosClient.interceptors.request.use(config => {
  const accessToken = authStore.getAccessToken();
  if (accessToken === null) {
    return config;
  }
  config.headers.common['Authorization'] = `Bearer ${accessToken}`;
  return config;
});

axiosClient.interceptors.response.use(
  response => {
    return response;
  },
  async error => {
    if (error.response.status === 401) {
      const tokensPayload = authStore.getTokens();

      if (tokensPayload.refresh === null) {
        return Promise.reject(error);
      }

      let refreshResponse;
      try {
        refreshResponse = await axios.post(
          '/auth/jwt/refresh/',
          tokensPayload,
          axiosConfig,
        );
      } catch (refreshError) {
        return Promise.reject(error);
      }
      authStore.login(refreshResponse.data);
      error.config.headers[
        'Authorization'
      ] = `Bearer ${refreshResponse.data.access}`;
      return axios(error.config);
    } else {
      return Promise.reject(error);
    }
  },
);

export default axiosClient;
