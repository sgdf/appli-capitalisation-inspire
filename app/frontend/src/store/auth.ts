import { reactive } from 'vue';
import { readonly } from '@vue/reactivity';
import { sessionStore } from './session';
import { Forum, UserData, UserExtraData } from '@/constants';

// TypeScript helpers
export type DeepReadOnly<Type> = {
  readonly [Key in keyof Type]: DeepReadOnly<Type[Key]>;
};

const notEmpty = <TValue>(
  value: TValue | null | undefined,
): value is TValue => {
  return value !== null && value !== undefined;
};

interface TokenInterface {
  exp: number;
  user_id: number;
  username: string;
  first_name: string;
  last_name: string;
}

const jwtDecrypt = (token: string): TokenInterface => {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split('')
      .map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join(''),
  );

  return JSON.parse(jsonPayload);
};

const isTokenAlive = (expiryDate: number | null) => {
  if (expiryDate === null) {
    return false;
  }
  return Date.now() < expiryDate * 1000;
};

export interface JwtData {
  access: string | null;
  refresh?: string | null;
}

type AuthState = {
  isLoggedIn: boolean;
  authData: {
    accessToken: string | null;
    refreshToken: string | null;
    tokenExp: number | null;
  };
  extraData: UserExtraData;
} & UserData;

/** Auth Store to store tokens and basic user data present in the access token
 *
 * This store follows the store pattern of Vue 3, as explained in:
 * - https://v3.vuejs.org/guide/state-management.html#simple-state-management-from-scratch
 * - https://medium.com/@mario.brendel1990/vue-3-the-new-store-a7569d4a546f
 */
export const authStore = {
  state: reactive<AuthState>({
    isLoggedIn: false,
    authData: {
      accessToken: null,
      refreshToken: null,
      tokenExp: null,
    },
    userId: null,
    firstName: null,
    lastName: null,
    username: null,
    forums: [],
    extraData: {
      age: null,
      territory: null,
      group: null,
      missions: null,
      groupTypology: null,
      seniority: null,
    },
  }),

  hasSubscribedToForum(themeId: number): boolean {
    return themeId !== null && this.getUserThemeIds().includes(themeId);
  },

  setUserForums(forums: Forum[]) {
    this.state.forums = forums;
    localStorage.setItem('forums', JSON.stringify(forums));
  },

  setUserExtraData(userExtraData: UserExtraData) {
    this.state.extraData = userExtraData;
    localStorage.setItem('userextradata', JSON.stringify(this.state.extraData));
  },

  isTokenActive() {
    if (!this.state.authData.tokenExp) {
      return false;
    }
    return isTokenAlive(this.state.authData.tokenExp);
  },

  isLoggedIn() {
    return this.state.isLoggedIn;
  },

  getAccessToken() {
    return this.state.authData.accessToken;
  },

  getTokens() {
    return readonly({
      access: this.state.authData.accessToken,
      refresh: this.state.authData.refreshToken,
    });
  },

  getUserId(): number | null {
    return this.state.userId;
  },

  getUserThemeIds(): Readonly<number[]> {
    return readonly(
      this.state.forums.map(forum => forum.theme).filter(notEmpty),
    );
  },

  getUserForums(): Readonly<Forum[]> {
    return readonly(this.state.forums.filter(notEmpty));
  },

  getUserData(): DeepReadOnly<UserData & UserExtraData> {
    return readonly({
      userId: this.state.userId,
      username: this.state.username,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      forums: this.state.forums,
      age: this.state.extraData.age,
      territory: this.state.extraData.territory,
      group: this.state.extraData.group,
      missions: this.state.extraData.missions,
      groupTypology: this.state.extraData.groupTypology,
      seniority: this.state.extraData.seniority,
    });
  },

  getFirstName(): string | null {
    return this.state.firstName;
  },

  login(jwtData: JwtData) {
    if (jwtData.access === null || jwtData.refresh === null) {
      this.logout();
      return;
    }
    let jwtDecodedValue;
    try {
      jwtDecodedValue = jwtDecrypt(jwtData.access);
    } catch (error) {
      console.warn('invalid token', error);
      this.logout();
      return;
    }
    this.state.authData.accessToken = jwtData.access;
    this.state.authData.tokenExp = jwtDecodedValue.exp;
    this.state.userId = jwtDecodedValue.user_id;
    this.state.username = jwtDecodedValue.username;
    this.state.firstName = jwtDecodedValue.first_name;
    this.state.lastName = jwtDecodedValue.last_name;
    // WARNING SECURITY: bad practice to store tokens in the unsecure local storage!
    localStorage.setItem('access_token', jwtData.access);
    // When refreshing token, only access token is returned, so refresh token is undefined.
    if (jwtData.refresh !== undefined) {
      this.state.authData.refreshToken = jwtData.refresh;
      localStorage.setItem('refresh_token', jwtData.refresh);
    }
    this.state.isLoggedIn = true;
  },

  logout() {
    this.state.authData = {
      accessToken: null,
      refreshToken: null,
      tokenExp: null,
    };
    this.state.userId = null;
    this.state.firstName = null;
    this.state.forums = [];
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    this.state.isLoggedIn = false;

    sessionStore.resetParticipation();
    localStorage.removeItem('aspirations');
    localStorage.removeItem('forums');
    localStorage.removeItem('userextradata');
  },
};
