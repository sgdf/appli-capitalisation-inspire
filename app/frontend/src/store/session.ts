import { reactive } from 'vue';

export enum ParticipationType {
  INDIVIDUAL = 'individual',
  UNIT = 'unit',
}

export enum ParticipationUnit {
  FARFA = 'Farfadets',
  LJ = 'Louveteaux-Jeannettes',
  SG = 'Scouts-Guides',
  PC = 'Pionniers-Caravelles',
}

export enum VisibilityType {
  FLJ = 'FLJ',
  SGPC = 'SGPC',
  YOUTH = 'YOUTH',
  FLJADULT = 'FLJADULT',
  SGPCADULT = 'SGPCADULT',
  ADULT = 'ADULT',
  ALL = 'ALL',
  FORUM = 'FORUM',
}

export interface SessionState {
  participationType: ParticipationType | null;
  participationUnit: ParticipationUnit | null;
}

/** Session Store
 *
 * This store follows the store pattern of Vue 3, as explained in:
 * - https://v3.vuejs.org/guide/state-management.html#simple-state-management-from-scratch
 * - https://medium.com/@mario.brendel1990/vue-3-the-new-store-a7569d4a546f
 */
export const sessionStore = {
  state: reactive<SessionState>({
    participationType: null,
    participationUnit: null,
  }),

  setParticipation(participationType: ParticipationType) {
    this.state.participationType = participationType;
    localStorage.setItem('participation', participationType);
    this.resetUnit();
  },

  resetParticipation() {
    this.state.participationType = null;
    localStorage.removeItem('participation');
    this.resetUnit();
  },

  setUnit(participationUnit: ParticipationUnit) {
    this.state.participationUnit = participationUnit;
    localStorage.setItem('unit', participationUnit);
    this.state.participationType = ParticipationType.UNIT;
    localStorage.setItem('participation', ParticipationType.UNIT);
  },
  resetUnit() {
    this.state.participationUnit = null;
    localStorage.removeItem('unit');
  },

  isVisible(visibleType: VisibilityType) {
    switch (visibleType) {
      case VisibilityType.FLJ:
        return [ParticipationUnit.FARFA, ParticipationUnit.LJ].includes(
          this.state.participationUnit as ParticipationUnit,
        );
      case VisibilityType.SGPC:
        return [ParticipationUnit.SG, ParticipationUnit.PC].includes(
          this.state.participationUnit as ParticipationUnit,
        );
      case VisibilityType.YOUTH:
        return [
          ParticipationUnit.FARFA,
          ParticipationUnit.LJ,
          ParticipationUnit.SG,
          ParticipationUnit.PC,
        ].includes(this.state.participationUnit as ParticipationUnit);
      case VisibilityType.FLJADULT:
        return (
          [ParticipationUnit.FARFA, ParticipationUnit.LJ].includes(
            this.state.participationUnit as ParticipationUnit,
          ) || this.state.participationType === ParticipationType.INDIVIDUAL
        );
      case VisibilityType.SGPCADULT:
        return (
          [ParticipationUnit.SG, ParticipationUnit.PC].includes(
            this.state.participationUnit as ParticipationUnit,
          ) || this.state.participationType === ParticipationType.INDIVIDUAL
        );
      case VisibilityType.ADULT:
        return this.state.participationType === ParticipationType.INDIVIDUAL;
      case VisibilityType.ALL:
        return true;
      case VisibilityType.FORUM:
        return false;
      default:
        return false;
    }
  },

  isParticipationValid(): boolean {
    return (
      (this.state.participationType === ParticipationType.INDIVIDUAL &&
        this.state.participationUnit === null) ||
      (this.state.participationType === ParticipationType.UNIT &&
        this.state.participationUnit !== null)
    );
  },

  getParticipationType(): Readonly<ParticipationType | null> {
    return this.state.participationType;
  },

  getParticipationName(): string {
    switch (this.state.participationType) {
      case ParticipationType.INDIVIDUAL:
        return 'Personnel';
      case ParticipationType.UNIT:
        return 'En unité';
      default:
        return ' - ';
    }
  },

  isParticipationDefined(): boolean {
    return this.state.participationType !== null;
  },
  getParticipationUnit(): Readonly<ParticipationUnit | null> {
    return this.state.participationUnit;
  },

  getParticipationUnitColor() {
    switch (this.state.participationUnit) {
      case ParticipationUnit.FARFA:
        return '#65bc99';
      case ParticipationUnit.LJ:
        return '#ff8300';
      case ParticipationUnit.SG:
        return '#0077b3';
      case ParticipationUnit.PC:
        return '#d03f15';
      default:
        return '#6c757d';
    }
  },

  isUnitDefined(): boolean {
    return this.state.participationUnit !== null;
  },
};
