import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { authStore } from '@/store/auth';
import ParticipationChoice from '@/views/ParticipationChoice.vue';
import Login from '@/views/Login.vue';
import ThemeChoice from '@/views/ThemeChoice.vue';
import FireCamp from '@/views/FireCamp.vue';
import MyUnit from '@/views/MyUnit.vue';
import {
  ParticipationType,
  ParticipationUnit,
  sessionStore,
} from '@/store/session';
import { Forum, UserExtraData } from '@/constants';

export enum Routes {
  REGISTER = 'Register',
  LOGIN = 'Login',
  THEME_CHOICE = 'ThemeChoice',
  PARTICIPATION_CHOICE = 'ParticipationChoice',
  THEME_PRESENTATION = 'ThemePresentation',
  THEME = 'Theme',
  ERROR_404 = 'Error404',
  MY_UNIT = 'MyUnit',
  USER_PROFILE = 'UserProfile',
  FIRECAMP = 'FireCamp',
}

const routes: RouteRecordRaw[] = [
  {
    path: '/choix',
    name: Routes.PARTICIPATION_CHOICE,
    component: ParticipationChoice,
  },
  {
    path: '/inscription',
    name: Routes.REGISTER,
    component: () => import('@/views/Register.vue'),
  },
  {
    path: '/',
    name: Routes.THEME_CHOICE,
    component: ThemeChoice,
  },
  {
    path: '/choix-unite',
    name: Routes.MY_UNIT,
    component: MyUnit,
  },
  {
    path: '/connexion',
    name: Routes.LOGIN,
    component: Login,
  },
  {
    path: '/thematiques',
    name: Routes.THEME_PRESENTATION,
    component: () => import('@/views/ThemePresentation.vue'),
  },
  {
    path: '/thematique/:themeId',
    name: Routes.THEME,
    component: () => import('@/views/Theme.vue'),
    props: (route: any) => {
      const themeId = Number.parseInt(route.params.themeId, 10);
      if (Number.isNaN(themeId)) {
        return 0; // TODO: 404
      }
      return { themeId };
    },
  },
  {
    path: '/profil',
    name: Routes.USER_PROFILE,
    component: () => import('@/views/UserProfile.vue'),
  },
  {
    path: '/thematique/:themeId/feu-de-camp',
    name: Routes.FIRECAMP,
    component: () => import('@/views/FireCamp.vue'),
    props: (route: any) => {
      const themeId = Number.parseInt(route.params.themeId, 10);
      if (Number.isNaN(themeId)) {
        return 0; // TODO: 404
      }
      return { themeId };
    },
  },
  // Error 404 must be the last route
  {
    path: '/:pathMatch(.*)*',
    name: Routes.ERROR_404,
    component: () => import('@/views/Error404.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

/** Before each location change:
 * - Populate stores with data saved in the localStorage
 * - Check login state and redirect to login page if needed
 *  */
router.beforeEach((to, from, next) => {
  // Restore session state
  const participationType = localStorage.getItem('participation');
  const participationUnit = localStorage.getItem('unit');
  if (participationType !== null) {
    sessionStore.setParticipation(participationType as ParticipationType);
  }
  if (participationUnit !== null) {
    sessionStore.setUnit(participationUnit as ParticipationUnit);
  }

  // Restore auth state
  // Restore forums
  const forums = localStorage.getItem('forums');
  if (forums !== null) {
    let parsedForums;
    try {
      parsedForums = JSON.parse(forums) as Forum[];
    } catch (error) {
      console.error('Corrupted value for "forums" in localStorage', forums);
      authStore.logout();
      localStorage.removeItem('forums');
      return next({ name: Routes.LOGIN });
    }
    authStore.setUserForums(parsedForums);
  }
  // Restore user extra data
  const userExtraData = localStorage.getItem('userextradata');
  if (userExtraData !== null) {
    let parsedUserExtraData;
    try {
      parsedUserExtraData = JSON.parse(userExtraData) as UserExtraData;
    } catch (error) {
      console.error(
        'Corrupted value for "userextradata" in userExtraData',
        forums,
      );
      authStore.logout();
      localStorage.removeItem('userextradata');
      return next({ name: Routes.LOGIN });
    }
    authStore.setUserExtraData(parsedUserExtraData);
  }
  // Restore auth
  if (!authStore.isLoggedIn()) {
    const access_token = localStorage.getItem('access_token');
    const refresh_token = localStorage.getItem('refresh_token');
    if (access_token) {
      const jwtData = {
        access: access_token,
        refresh: refresh_token,
      };
      authStore.login(jwtData);
    }
  }

  // if user is still not logged in, redirect to login page
  if (!authStore.isLoggedIn()) {
    if (
      !([
        Routes.LOGIN,
        Routes.REGISTER,
        Routes.THEME_PRESENTATION,
      ] as any[]).includes(to.name)
    ) {
      return next({ name: Routes.LOGIN, params: { nextUrl: to.fullPath } });
    }
  }

  return next();
});

export default router;
