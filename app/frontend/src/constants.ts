import { VisibilityType } from './store/session';

export enum QuestionType {
  TEXT = 'TEXT',
  QCU = 'QCU',
  ASPIRATION_QUESTION = 'ASPIRATION_QUESTION',
  LIST_TO_SORT = 'LIST_TO_SORT',
}

export interface Question {
  id: number;
  text: string;
  type: QuestionType;
  /** Optional content id */
  content: number | null;
  /** Optional theme id */
  theme: number | null;
  content_details: {
    theme: {
      id: number;
      prioritized_aspirations: [
        {
          id: number;
          type?: string;
          title: string;
          description: string;
        },
      ];
    };
  } | null;
}

export interface QcuQuestion {
  id: number;
  questionText: string;
  possibleAnswers: string[];
  defaultAnswer?: string;
}
export interface OrderList {
  id: number;
  questionText: string;
  listToOrder: { id: number; type?: string; title: string }[];
}

export interface Answer {
  id: number;
  text: string;
  /** question id */
  question: number;
}

export enum ContentType {
  PODCAST = 'PODCAST',
  DOCUMENT = 'DOCUMENT',
  ASPIRATION = 'ASPIRATION',
  PRIORITIZED_ASPIRATION = 'PRIORITIZED_ASPIRATION',
}

export enum SubContentType {
  AUDIO = 'AUDIO',
  VIDEO = 'VIDEO',
  FILE = 'FILE',
  LINK = 'LINK',
  NONE = 'NONE',
}

export interface Content {
  id: number;
  content_type: ContentType;
  title: string;
  description: string;
  url: string;
  sub_content: SubContentType;
  visibility: VisibilityType;
  theme: number | null;
}

export interface Document extends Omit<Content, 'content_type'> {
  content_type: ContentType.DOCUMENT;
}

export interface Theme {
  id: number;
  name: string;
  description: string;
  contents: Content[];
  forum: number | null;
  date: string | null;
}

export interface Forum {
  id: number;
  theme: number | null;
  url: string;
  description: string;
  name: string | null;
}

export interface UserData {
  userId: number | null;
  username: string | null;
  firstName: string | null;
  lastName: string | null;
  forums: Forum[];
}

export interface UserExtraData {
  age: string | null;
  territory: string | null;
  group: string | null;
  missions: string | null;
  groupTypology: string | null;
  seniority: string | null;
}

export type UserExtraRawData = Omit<UserExtraData, 'groupTypology'> & {
  group_typology: string | null;
};
export interface ForumAspiration {
  id: number;
  title: string;
  description: string;
  theme: number;
}

export interface ForumAspirationAnswer {
  id?: number;
  aspirationId: number;
  groupName: string;
  prosArguments: string;
  consArguments: string;
  amendment: string;
}

export interface ForumAspirationAnswerAPI {
  id: number;
  aspiration_id: number;
  group_name: string;
  pros_arguments: string;
  cons_arguments: string;
  amendment: string;
}
