# App

There are 2 parts in the Inspire App:
* [backend](backend/README.md): Django REST JSON API with Postgre database. 
* [frontend](frontend/README.md): VueJs UI