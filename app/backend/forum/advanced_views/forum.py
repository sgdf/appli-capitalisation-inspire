from rest_framework import serializers, viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework_extensions.mixins import NestedViewSetMixin

from forum.models import Forum, User
from forum.permissions import ReadOnly


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "is_staff",
            "is_active",
            "email",
            "first_name",
            "last_name",
            "territory",
            "group",
            "missions",
        ]


class ForumDataSerializer(serializers.ModelSerializer):
    name = serializers.SlugRelatedField(
        read_only=True, slug_field="name", source="theme"
    )
    users = UserSerializer(many=True, read_only=True)

    class Meta:
        model = Forum
        fields = ["id", "theme_id", "name", "description", "url", "date", "users"]


class ForumDataViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Forum.objects.all()
    serializer_class = ForumDataSerializer
    permission_classes = [IsAdminUser, ReadOnly]


class SimpleForumDataSerializer(serializers.ModelSerializer):
    name = serializers.SlugRelatedField(
        read_only=True, slug_field="name", source="theme"
    )
    users = serializers.SlugRelatedField(read_only=True, slug_field="email", many=True)

    class Meta:
        model = Forum
        fields = ["id", "theme_id", "name", "date", "users"]


class SimpleForumDataViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Forum.objects.all()
    serializer_class = SimpleForumDataSerializer
    permission_classes = [IsAdminUser, ReadOnly]
