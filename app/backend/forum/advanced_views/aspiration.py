from rest_framework import serializers, viewsets, permissions
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from forum.models import ForumAspiration
from forum.permissions import ReadOnly

from helpers import can_be_casted_to_integer
from settings import ALLOW_FORUM_ASPIRATIONS


class ForumAspirationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ForumAspiration
        fields = "__all__"


class DataForumAspirationViewSet(viewsets.ModelViewSet):
    queryset = ForumAspiration.objects.all()
    serializer_class = ForumAspirationSerializer
    permission_classes = [
        IsAdminUser,
        ReadOnly,
    ]


class ForumAspirationViewSet(viewsets.ModelViewSet):
    queryset = ForumAspiration.objects.all()
    serializer_class = ForumAspirationSerializer
    permission_classes = [
        IsAuthenticated if ALLOW_FORUM_ASPIRATIONS else IsAdminUser,
        ReadOnly,
    ]

    def get_queryset(self):
        """
        Filters on theme_id
        """
        queryset = ForumAspiration.objects.all()
        user_theme_ids = [forum.theme_id for forum in self.request.user.forums.all()]
        theme_id = self.request.query_params.get("theme_id")
        if can_be_casted_to_integer(theme_id):
            queryset = queryset.filter(theme_id=theme_id, theme_id__in=user_theme_ids)
        return queryset.filter(theme_id__in=user_theme_ids)
