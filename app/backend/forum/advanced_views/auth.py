from django.db import transaction
import djoser.serializers
import djoser.conf
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from forum.models import User, Forum, Theme
from logger import logger


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user: User):
        token = super().get_token(user)

        # Add custom claims
        token["username"] = user.username
        token["first_name"] = user.first_name
        token["last_name"] = user.last_name

        logger.info(
            f"[LOGIN] User #{user.id} ({user.username}) got tokens [is_staff:{user.is_staff}] [is_active:{user.is_active}]"
        )
        return token

    def validate(self, attrs):
        logger.info(f"[LOGIN] User {attrs.get(self.username_field)} trying to log in")
        return super().validate(attrs)


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


class CustomUserCreateSerializer(djoser.serializers.UserCreateSerializer):
    # Use theme ids to register forums
    themes = serializers.SlugRelatedField(
        many=True, queryset=Forum.objects.all(), slug_field="theme_id", source="forums"
    )

    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            djoser.conf.settings.LOGIN_FIELD,
            djoser.conf.settings.USER_ID_FIELD,
            "password",
            "first_name",
            "last_name",
            "age",
            "territory",
            "group",
            "missions",
            "group_typology",
            "seniority",
            "themes",
        )

    def create(self, validated_data):
        forums = validated_data.pop("forums")
        with transaction.atomic():
            user = super().create(validated_data)
            for forum in forums:
                forum.users.add(user)
        logger.info(f"[REGISTRATION] User {user.id} registered")
        return user

    def validate(self, attrs):
        logger.info(
            f"[REGISTRATION] Registration attempt for username {attrs.get('username', 'unknown')}"
        )
        forums = attrs.pop("forums")
        attrs = super().validate(attrs)
        attrs["forums"] = forums
        return attrs


# TODO: move views to folders to avoid duplicates like this one
class ForumSerializer(serializers.ModelSerializer):
    name = serializers.SlugRelatedField(
        read_only=True, slug_field="name", source="theme"
    )

    class Meta:
        model = Forum
        fields = ["id", "description", "url", "date", "theme", "name"]


class CustomCurrentUserSerializer(djoser.serializers.UserSerializer):
    forums = ForumSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            djoser.conf.settings.LOGIN_FIELD,
            djoser.conf.settings.USER_ID_FIELD,
            "first_name",
            "last_name",
            "age",
            "territory",
            "group",
            "missions",
            "group_typology",
            "seniority",
            "forums",
        )
        read_only_fields = (djoser.conf.settings.LOGIN_FIELD,)
