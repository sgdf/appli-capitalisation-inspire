from django.db import transaction

# Create your views here.
from rest_framework import serializers, viewsets, status
from rest_framework.exceptions import ValidationError, PermissionDenied
from rest_framework.response import Response

from forum.models import ForumAspirationAnswer, Theme
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser,
)
from forum.permissions import ReadOnly

from helpers import can_be_casted_to_integer
from settings import ALLOW_FORUM_ASPIRATIONS


class ForumAspirationAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ForumAspirationAnswer
        fields = [
            "id",
            "aspiration_id",
            "group_name",
            "pros_arguments",
            "cons_arguments",
            "amendment",
        ]


class DataForumAspirationAnswerViewSet(viewsets.ModelViewSet):
    """ForumAspirationAnswer view to get all answers

    GET: all aspiration answers
    """

    http_method_names = ["get"]
    queryset = ForumAspirationAnswer.objects.all()
    serializer_class = ForumAspirationAnswerSerializer
    permission_classes = [IsAdminUser, ReadOnly]


class ForumAspirationAnswerViewSet(viewsets.ModelViewSet):
    """ForumAspirationAnswer view to get all answers

    GET: all aspiration answers
    """

    http_method_names = ["get"]
    queryset = ForumAspirationAnswer.objects.all()
    serializer_class = ForumAspirationAnswerSerializer
    permission_classes = [
        IsAuthenticated if ALLOW_FORUM_ASPIRATIONS else IsAdminUser,
        ReadOnly,
    ]

    def get_queryset(self):
        """Filter on user_id"""
        queryset = ForumAspirationAnswer.objects.all()
        theme_id = self.request.query_params.get("theme_id")
        if not can_be_casted_to_integer(theme_id):
            raise ValidationError("Theme id should be an integer")
        user_forum_ids = [forum.id for forum in self.request.user.forums.all()]
        try:
            current_theme = Theme.objects.get(id=theme_id)
            current_forum_id = current_theme.forum.id
            if current_forum_id not in user_forum_ids:
                raise PermissionDenied(
                    f"User not registered to forum linked to theme {theme_id}"
                )
        except AttributeError:
            raise ValidationError("Forum may not exist")
        return queryset.filter(aspiration__theme_id=theme_id)


class UserForumAspirationAnswerViewSet(viewsets.ModelViewSet):
    """UserForumAspirationAnswer view for users

    GET: only user answers

    POST: only user answers + data as array of answer object (text + question fields)
    """

    http_method_names = ["get", "post"]
    queryset = ForumAspirationAnswer.objects.all()
    serializer_class = ForumAspirationAnswerSerializer
    permission_classes = [IsAuthenticated if ALLOW_FORUM_ASPIRATIONS else IsAdminUser]

    def get_queryset(self):
        """Filter on user_id"""
        queryset = ForumAspirationAnswer.objects.all()
        user_id = self.request.user.id

        theme_id = self.request.query_params.get("theme_id", None)
        if theme_id is None:
            return queryset.filter(
                user_id=user_id,
            )
        return queryset.filter(user_id=user_id, aspiration__theme_id=theme_id)

    @staticmethod
    def _custom_create_or_update(request, *args, **kwargs):
        data = request.data
        user_id = request.user.id
        data_answer = []
        # NB: bulk "update_or_create" does not exist, even if PostgreSQL supports ON CONFLICT in queries.
        # Therefore, it should be possible to enhance performance here
        with transaction.atomic():
            saved_answer = ForumAspirationAnswer.objects.update_or_create(
                data,
                aspiration_id=data["aspiration_id"],
                user_id=user_id,
            )
            data_answer.append(saved_answer[0].id)

        return Response(
            {"message": "success", "aspiration_ids": data_answer},
            status=status.HTTP_201_CREATED,
        )

    def create(self, request, *args, **kwargs):
        return self._custom_create_or_update(request, *args, **kwargs)
