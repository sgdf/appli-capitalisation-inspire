# Generated by Django 3.2 on 2021-05-07 22:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("forum", "0031_alter_content_visibility"),
    ]

    operations = [
        migrations.AlterField(
            model_name="answer",
            name="participation_type",
            field=models.CharField(
                choices=[
                    ("individual", "Individual"),
                    ("unit", "Unit"),
                    ("forum", "Forum"),
                ],
                default="individual",
                max_length=120,
            ),
        ),
    ]
