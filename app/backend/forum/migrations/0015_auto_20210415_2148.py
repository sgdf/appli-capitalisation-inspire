# Generated by Django 3.1.7 on 2021-04-15 21:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("forum", "0014_auto_20210411_2055"),
    ]

    operations = [
        migrations.AddField(
            model_name="answer",
            name="participation_type",
            field=models.CharField(
                choices=[("individual", "Individual"), ("unit", "Unit")],
                max_length=120,
                null=True,
            ),
        ),
        migrations.AlterUniqueTogether(
            name="answer",
            unique_together={("question", "user", "participation_type")},
        ),
    ]
