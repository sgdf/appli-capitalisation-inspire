# Generated by Django 3.2 on 2021-04-22 22:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("forum", "0018_auto_20210418_1759"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="seniority",
            field=models.CharField(
                choices=[
                    ("1", "Less Than One"),
                    ("1-2", "One To Two"),
                    ("3-5", "Three To Five"),
                    ("6-10", "Six To Ten"),
                    ("11-15", "Eleven To Fifteen"),
                    ("16-20", "Sixteen To Twenty"),
                    ("21-30", "Twenty One To Thirty"),
                    ("31-50", "Thirty One To Fifty"),
                    ("50+", "More Than Fifty"),
                ],
                max_length=120,
                null=True,
            ),
        ),
    ]
