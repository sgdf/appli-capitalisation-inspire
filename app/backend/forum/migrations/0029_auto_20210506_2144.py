# Generated by Django 3.2 on 2021-05-06 21:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("forum", "0028_theme_description"),
    ]

    operations = [
        migrations.AlterField(
            model_name="content",
            name="content_type",
            field=models.CharField(
                choices=[
                    ("PODCAST", "Podcast"),
                    ("DOCUMENT", "Document"),
                    ("ASPIRATION", "Aspiration"),
                ],
                default="PODCAST",
                max_length=120,
            ),
        ),
        migrations.AlterField(
            model_name="content",
            name="sub_content",
            field=models.CharField(
                choices=[
                    ("AUDIO", "Audio"),
                    ("VIDEO", "Video"),
                    ("FILE", "File"),
                    ("LINK", "Link"),
                    ("NONE", "None"),
                ],
                default="NONE",
                max_length=120,
            ),
        ),
        migrations.AlterField(
            model_name="question",
            name="type",
            field=models.CharField(
                choices=[
                    ("TEXT", "Text"),
                    ("QCU", "Qcu"),
                    ("ASPIRATION_QUESTION", "Aspiration Question"),
                ],
                default="TEXT",
                max_length=120,
            ),
        ),
    ]
