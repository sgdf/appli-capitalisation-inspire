# Generated by Django 3.1.7 on 2021-04-05 16:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("forum", "0005_auto_20210403_1312"),
    ]

    operations = [
        migrations.AddField(
            model_name="content",
            name="content_type",
            field=models.CharField(
                choices=[
                    ("PODCAST", "Podcast"),
                    ("CONFERENCE", "Conference"),
                    ("OTHER", "Other"),
                ],
                default="OTHER",
                max_length=120,
            ),
        ),
    ]
