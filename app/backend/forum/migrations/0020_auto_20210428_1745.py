# Generated by Django 3.2 on 2021-04-28 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("forum", "0019_alter_user_seniority"),
    ]

    operations = [
        migrations.AlterField(
            model_name="content",
            name="content_type",
            field=models.CharField(
                choices=[("PODCAST", "Podcast"), ("DOCUMENT", "Document")],
                default="PODCAST",
                max_length=120,
            ),
        ),
        migrations.AlterField(
            model_name="content",
            name="sub_content",
            field=models.CharField(
                choices=[("AUDIO", "Audio"), ("VIDEO", "Video"), ("FILE", "File")],
                default="AUDIO",
                max_length=120,
            ),
        ),
    ]
