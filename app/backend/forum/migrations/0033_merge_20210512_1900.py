# Generated by Django 3.2 on 2021-05-12 19:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("forum", "0029_alter_user_forums"),
        ("forum", "0032_alter_answer_participation_type"),
    ]

    operations = []
