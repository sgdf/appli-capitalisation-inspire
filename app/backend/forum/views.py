from typing import Tuple, Optional, List

from django.db import transaction
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView

# Create your views here.
from rest_framework import serializers, viewsets, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from forum.models import Theme, Content, Question, Answer, User, Forum
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser,
)
from rest_framework_extensions.mixins import NestedViewSetMixin

from forum.models.prioritized_aspiration import PrioritizedAspiration
from forum.permissions import ReadOnly
from helpers import can_be_casted_to_integer

# Import views
from forum.advanced_views import *


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = "__all__"


class ContentViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Content.objects.all()
    serializer_class = ContentSerializer
    permission_classes = [IsAuthenticated, ReadOnly]


# Serializers define the API representation.
class ThemeSerializer(serializers.ModelSerializer):
    contents = ContentSerializer(many=True, read_only=True)
    forum = serializers.PrimaryKeyRelatedField(read_only=True)
    date = serializers.SlugRelatedField(
        read_only=True, slug_field="date", source="forum"
    )

    class Meta:
        model = Theme
        fields = [
            "id",
            "name",
            "description",
            "forum",
            "date",
            "contents",
        ]


# ViewSets define the view behavior.
class ThemeViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer
    permission_classes = [ReadOnly]


class PrioritizedAspirationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrioritizedAspiration
        fields = ["id", "title", "description", "type"]


class ThemeWithPrioritizedAspirationsSerializer(serializers.ModelSerializer):
    prioritized_aspirations = PrioritizedAspirationsSerializer(
        many=True, read_only=True
    )

    class Meta:
        model = Theme
        fields = ["id", "prioritized_aspirations"]


class ContentWithPrioritizedAspirationsSerializer(serializers.ModelSerializer):
    theme = ThemeWithPrioritizedAspirationsSerializer(read_only=True)

    class Meta:
        model = Theme
        fields = ["theme"]


class QuestionSerializer(serializers.ModelSerializer):
    content_details = ContentWithPrioritizedAspirationsSerializer(
        read_only=True, source="content"
    )

    class Meta:
        model = Question
        fields = "__all__"


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [IsAuthenticated, ReadOnly]

    def get_queryset(self):
        """
        Filters on theme_id and content_id
        """
        queryset = Question.objects.all()
        theme_id = self.request.query_params.get("theme_id")
        content_id = self.request.query_params.get("content_id")
        if can_be_casted_to_integer(theme_id):
            queryset = queryset.filter(theme_id=theme_id)
        if can_be_casted_to_integer(content_id):
            queryset = queryset.filter(content_id=content_id)
        return queryset


class AnswerDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = "__all__"


class AnswerDataViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    """Answer view for data analysis"""

    queryset = Answer.objects.all()
    serializer_class = AnswerDataSerializer
    # Only admin users can retrieve all answers
    permission_classes = [IsAdminUser, ReadOnly]


class UserAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ["id", "text", "participation_type", "participation_unit", "question"]


def set_participation_type(user_forum_ids: List[int], question_id: int, answer) -> None:
    try:
        current_question = Question.objects.get(id=question_id)
        current_forum_id = current_question.content.theme.forum.id
        is_forum_content = (
            current_question.content.content_type == Content.ContentType.ASPIRATION
        )
    except AttributeError:
        return
    if current_forum_id in user_forum_ids and is_forum_content:
        answer["participation_type"], answer["participation_unit"] = (
            Answer.ParticipationType.FORUM,
            None,
        )


class UserAnswerViewSet(viewsets.ModelViewSet):
    """Answer view for users

    GET: only user answers + "qid" query param to select questions

    POST: only user answers + data as array of answer object (text + question fields)
    """

    http_method_names = ["get", "post"]
    queryset = Answer.objects.all()
    serializer_class = UserAnswerSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter on user_id"""
        queryset = Answer.objects.all()
        user_id = self.request.user.id
        participation_type = self.request.query_params.get("participation_type")
        participation_unit = self.request.query_params.get("participation_unit")
        if not participation_unit:
            participation_unit = None
        if (
            participation_type is None
            or (not participation_unit and participation_type == "unit")
            or (participation_unit and participation_type == "individual")
        ):
            raise ValidationError("Participation type and unit are incorrect!")
        question_ids = self.request.query_params.getlist("qid")
        if question_ids:
            return queryset.filter(
                user_id=user_id,
                question_id__in=question_ids,
                participation_type=participation_type,
                participation_unit=participation_unit,
            )
        return queryset.filter(
            user_id=user_id,
            participation_type=participation_type,
            participation_unit=participation_unit,
        )

    @staticmethod
    def _custom_create_or_update(request, *args, **kwargs):
        data = request.data
        if isinstance(data, dict):
            data = [data]
        if not isinstance(data, list):
            raise ValidationError("Data must be an answer or a list of answers")
        user_id = request.user.id
        user_forum_ids = [forum.id for forum in request.user.forums.all()]
        data_answer = []
        # NB: bulk "update_or_create" does not exist, even if PostgreSQL supports ON CONFLICT in queries.
        # Therefore, it should be possible to enhance performance here
        with transaction.atomic():
            for i, answer in enumerate(data):
                question_id = answer["question_id"]
                set_participation_type(user_forum_ids, question_id, answer)
                saved_answer = Answer.objects.update_or_create(
                    answer,
                    participation_type=answer["participation_type"],
                    participation_unit=answer["participation_unit"],
                    question_id=question_id,
                    user_id=user_id,
                )
                data_answer.append(saved_answer[0].id)

        return Response(
            {"message": "success", "answer_ids": data_answer},
            status=status.HTTP_201_CREATED,
        )

    def create(self, request, *args, **kwargs):
        return self._custom_create_or_update(request, *args, **kwargs)


# User serializer and view for data analysis
class UserDataSerializer(serializers.ModelSerializer):
    themes = serializers.SlugRelatedField(
        many=True, queryset=Forum.objects.all(), slug_field="theme_id", source="forums"
    )

    class Meta:
        model = User
        fields = [
            "id",
            "is_staff",
            "is_active",
            "age",
            "territory",
            "group",
            "missions",
            "group_typology",
            "seniority",
            "themes",
        ]


class UserDataViewSet(viewsets.ModelViewSet):
    """User view for data analysis"""

    queryset = User.objects.all()
    serializer_class = UserDataSerializer
    # Only admin users can retrieve all answers
    permission_classes = [IsAdminUser, ReadOnly]
