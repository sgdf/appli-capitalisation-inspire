from django.db.utils import IntegrityError
from forum.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Create a new user in the DB. Arguments must be -u [Username] -p [Password]"

    def add_arguments(self, parser):
        parser.add_argument(
            "-u",
            "--user",
            help="Username",
        )
        parser.add_argument(
            "-p",
            "--password",
            help="Password",
        )

    def handle(self, *args, **options):
        try:
            User.objects.create_superuser(
                options["user"],
                "",
                options["password"],
            )
            print(f"User account {options['user']} created!")
        except IntegrityError as err:
            print(
                f"Error while creating user! Maybe it already exists?\nOriginal error: {err}"
            )
