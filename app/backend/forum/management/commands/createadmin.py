from django.db.utils import IntegrityError
from forum.models import User
from django.core.management.base import BaseCommand

import environ

# Initialise environment variables
env = environ.Env()
environ.Env.read_env()


class Command(BaseCommand):
    help = "Create a super admin in the DB. Credentials are in .env"

    def handle(self, *args, **options):
        try:
            User.objects.create_superuser(
                env("DJANGO_SUPERUSER_NAME"),
                "",
                env("DJANGO_SUPERUSER_PASSWORD"),
                first_name="SGDF - admin",
            )
            print("Admin account created!")
        except IntegrityError as err:
            print(
                f"Error while creating superuser! Maybe it already exists?\nOriginal error: {err}"
            )
