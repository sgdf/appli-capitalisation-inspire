from django.db import models


class ForumAspirationAnswer(models.Model):
    amendment = models.TextField(blank=True)
    group_name = models.TextField(blank=True)
    pros_arguments = models.TextField(blank=True)
    cons_arguments = models.TextField(blank=True)

    user = models.ForeignKey("User", on_delete=models.SET_NULL, null=True)

    aspiration = models.ForeignKey(
        "ForumAspiration", related_name="answers", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return f"{self.id} - Groupe {self.group_name} - [Aspiration: {self.aspiration}]"
