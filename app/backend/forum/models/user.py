from django.db import models
from django.contrib.auth.models import AbstractUser

from forum.models.forum import Forum

# User model


class User(AbstractUser):
    class GroupTypology(models.TextChoices):
        __empty__ = "---"
        CITY = "urbain"
        SUBURBS = "periurbain"
        COUNTRYSIDE = "rural"
        TERRITORY = "territoire"
        NATIONAL = "national"

    class Seniority(models.TextChoices):
        __empty__ = "---"
        LESS_THAN_ONE = "1"
        ONE_TO_TWO = "1-2"
        THREE_TO_FIVE = "3-5"
        SIX_TO_TEN = "6-10"
        ELEVEN_TO_FIFTEEN = "11-15"
        SIXTEEN_TO_TWENTY = "16-20"
        TWENTY_ONE_TO_THIRTY = "21-30"
        THIRTY_ONE_TO_FIFTY = "31-50"
        MORE_THAN_FIFTY = "50+"

    # fields that already exist: first_name, last_name, email, password
    age = models.IntegerField(null=True)
    territory = models.CharField(max_length=120, blank=True)
    group = models.CharField(max_length=120, blank=True)
    missions = models.CharField(max_length=120, blank=True)
    group_typology = models.CharField(
        max_length=120, choices=GroupTypology.choices, blank=True
    )
    seniority = models.CharField(max_length=120, choices=Seniority.choices, blank=True)

    forums = models.ManyToManyField(Forum, related_name="users", blank=True)

    def __str__(self):
        return f"{self.id} - {self.username} - forums: {[forum.id for forum in self.forums.all()]}"
