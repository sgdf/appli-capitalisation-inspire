from django.db import models

# Theme model


class Theme(models.Model):
    name = models.TextField()
    description = models.TextField(blank=True)

    def __str__(self):
        return f"{self.id} - {self.name}"
