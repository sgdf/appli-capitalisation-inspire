from django.db import models

# Forum model


class Forum(models.Model):
    description = models.TextField(blank=True)
    url = models.URLField(blank=True)
    # A forum must be linked to a unique theme
    theme = models.OneToOneField(
        "Theme", related_name="forum", on_delete=models.CASCADE, null=True
    )
    date = models.TextField(blank=True)

    def __str__(self):
        return f"{self.id} (Theme: {self.theme}) - {self.description[:30]}"
