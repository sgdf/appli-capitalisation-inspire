from django.db import models


class PrioritizedAspiration(models.Model):
    title = models.TextField()
    description = models.TextField(blank=True)
    type = models.TextField(blank=True)

    theme = models.ForeignKey(
        "Theme",
        related_name="prioritized_aspirations",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.id} (theme {self.theme} | {self.type}) - {self.title}"
