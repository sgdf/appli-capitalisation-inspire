from django.db import models

# Answer model


class Answer(models.Model):
    class ParticipationType(models.TextChoices):
        INDIVIDUAL = "individual"
        UNIT = "unit"
        FORUM = "forum"

    class ParticipationUnit(models.TextChoices):
        FARFA = "Farfadets"
        LJ = "Louveteaux-Jeannettes"
        SG = "Scouts-Guides"
        PC = "Pionniers-Caravelles"

    # If questions are deleted, all answers are deleted
    question = models.ForeignKey("Question", on_delete=models.CASCADE)
    # If users are deleted, all their questions are deleted
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    text = models.TextField(blank=True)

    participation_type = models.CharField(
        max_length=120,
        choices=ParticipationType.choices,
        default=ParticipationType.INDIVIDUAL,
    )
    participation_unit = models.CharField(
        max_length=120, choices=ParticipationUnit.choices, null=True, blank=True
    )

    class Meta:
        unique_together = (
            "question",
            "user",
            "participation_type",
            "participation_unit",
        )

    def __str__(self):
        return f"{self.id} (Question {self.question_id} / User {self.user_id}) - {self.text}"
