from django.db import models

# Content model


class Content(models.Model):
    class ContentType(models.TextChoices):
        PODCAST = "PODCAST"
        DOCUMENT = "DOCUMENT"
        ASPIRATION = "ASPIRATION"
        PRIORITIZED_ASPIRATION = "PRIORITIZED_ASPIRATION"

    class VisibilityType(models.TextChoices):
        FLJ = "FLJ"
        SGPC = "SGPC"
        YOUTH = "YOUTH"
        FLJADULT = "FLJADULT"
        SGPCADULT = "SGPCADULT"
        ADULT = "ADULT"
        ALL = "ALL"
        FORUM = "FORUM"

    class SubContentType(models.TextChoices):
        AUDIO = "AUDIO"
        VIDEO = "VIDEO"
        FILE = "FILE"
        LINK = "LINK"
        NONE = "NONE"

    theme = models.ForeignKey(
        "Theme", related_name="contents", on_delete=models.CASCADE, null=True
    )
    content_type = models.CharField(
        max_length=120, choices=ContentType.choices, default=ContentType.PODCAST
    )
    title = models.TextField()
    description = models.TextField(blank=True, default="")
    url = models.URLField(blank=True, default="")
    sub_content = models.CharField(
        max_length=120, choices=SubContentType.choices, default=SubContentType.NONE
    )
    visibility = models.CharField(
        max_length=120, choices=VisibilityType.choices, default=VisibilityType.ALL
    )

    def __str__(self):
        return f"{self.id} (theme {self.theme_id} / type {self.content_type} [{self.sub_content}]) [{self.visibility}] - {self.title}"
