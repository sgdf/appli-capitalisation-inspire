from django.db import models


class ForumAspiration(models.Model):
    title = models.TextField()
    description = models.TextField(blank=True)

    theme = models.ForeignKey(
        "Theme", related_name="aspirations", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return f"{self.id} (theme {self.theme}) - {self.title}"
