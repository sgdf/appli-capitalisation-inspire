from django.db import models

# Question model


class Question(models.Model):
    class QuestionType(models.TextChoices):
        TEXT = "TEXT"
        QCU = "QCU"
        ASPIRATION_QUESTION = "ASPIRATION_QUESTION"
        LIST_TO_SORT = "LIST_TO_SORT"

    content = models.ForeignKey(
        "Content",
        related_name="questions",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    theme = models.ForeignKey(
        "Theme",
        related_name="questions",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    text = models.TextField()
    type = models.CharField(
        max_length=120, choices=QuestionType.choices, default=QuestionType.TEXT
    )

    def __str__(self):
        return f"{self.id} (theme {self.theme_id} / content {self.content_id}) - {self.text}"
