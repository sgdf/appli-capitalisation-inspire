from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ExportActionModelAdmin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms

# Register your models here.

from .models import (
    User,
    Theme,
    Content,
    Question,
    Answer,
    Forum,
    ForumAspiration,
    ForumAspirationAnswer,
)
from .models.prioritized_aspiration import PrioritizedAspiration


class UserResource(resources.ModelResource):
    class Meta:
        model = User


class UserCreationFormExtended(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["age"] = forms.IntegerField(label="Age", min_value=0)
        self.fields["territory"] = forms.CharField(
            label="Territoire", max_length=120, required=False
        )
        self.fields["group"] = forms.CharField(
            label="Groupe", max_length=120, required=False
        )
        self.fields["missions"] = forms.CharField(label="Mission", max_length=120)
        self.fields["group_typology"] = forms.ChoiceField(
            label="Typologie de groupe",
            choices=User.GroupTypology.choices,
            required=False,
        )
        self.fields["seniority"] = forms.ChoiceField(
            label="Ancienneté",
            choices=User.Seniority.choices,
            required=False,
        )
        self.fields["forums"] = forms.ModelMultipleChoiceField(
            Forum.objects.get_queryset(), required=False
        )


class UserChangeFormExtended(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["age"] = forms.IntegerField(
            label="Age", min_value=0, required=False
        )
        self.fields["territory"] = forms.CharField(
            label="Territoire", max_length=120, required=False
        )
        self.fields["group"] = forms.CharField(
            label="Groupe", max_length=120, required=False
        )
        self.fields["missions"] = forms.CharField(
            label="Mission", max_length=120, required=False
        )
        self.fields["group_typology"] = forms.ChoiceField(
            label="Typologie de groupe",
            choices=User.GroupTypology.choices,
            required=False,
        )
        self.fields["seniority"] = forms.ChoiceField(
            label="Ancienneté",
            choices=User.Seniority.choices,
            required=False,
        )
        self.fields["forums"] = forms.ModelMultipleChoiceField(
            Forum.objects.get_queryset(), required=False
        )


@admin.register(User)
class UserAdmin(DjangoUserAdmin, ImportExportModelAdmin):
    list_display = [*DjangoUserAdmin.list_display, "date_joined"]
    resource_class = UserResource
    add_form = UserCreationFormExtended
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "username",
                    "password1",
                    "password2",
                    "age",
                    "territory",
                    "group",
                    "missions",
                    "group_typology",
                    "seniority",
                    "forums",
                ),
            },
        ),
    )
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            "Personal info",
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "age",
                    "territory",
                    "group",
                    "missions",
                    "group_typology",
                    "seniority",
                    "forums",
                )
            },
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        ("Important dates", {"fields": ("last_login", "date_joined")}),
    )
    form = UserChangeFormExtended
    ordering = ("is_staff", "username")


class ThemeResource(resources.ModelResource):
    class Meta:
        model = Theme


@admin.register(Theme)
class ThemeAdmin(ImportExportModelAdmin):
    resource_class = ThemeResource
    list_filter = ["name"]
    search_fields = ["name"]


class ContentResource(resources.ModelResource):
    class Meta:
        model = Content


@admin.register(Content)
class ContentAdmin(ImportExportModelAdmin):
    resource_class = ContentResource
    list_display = ("title", "id", "theme", "content_type", "sub_content", "visibility")
    list_filter = ["theme", "content_type", "sub_content"]
    search_fields = ["title", "description", "url"]


class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question


@admin.register(Question)
class QuestionAdmin(ImportExportModelAdmin):
    resource_class = QuestionResource
    list_display = ("text", "id", "content", "theme", "type")
    list_filter = [
        "theme",
        "type",
        "content__content_type",
        "content",
    ]
    search_fields = ["text"]


class AnswerResource(resources.ModelResource):
    class Meta:
        model = Answer


@admin.register(Answer)
class AnswerAdmin(ImportExportModelAdmin):
    resource_class = AnswerResource
    list_display = ("__str__", "question", "participation_type", "participation_unit")
    list_filter = [
        "participation_type",
        "participation_unit",
        "question__type",
        "question__content__content_type",
        "question__content__theme",
        "question__content",
        "question",
    ]


class ForumResource(resources.ModelResource):
    class Meta:
        model = Forum


@admin.register(Forum)
class ForumAdmin(ImportExportModelAdmin):
    resource_class = ForumResource
    list_display = ("theme", "date", "url", "description")


class ForumAspirationResource(resources.ModelResource):
    class Meta:
        model = ForumAspiration


@admin.register(ForumAspiration)
class ForumAspirationAdmin(ImportExportModelAdmin):
    resource_class = ForumAspirationResource
    list_display = ("id", "title", "theme")
    list_filter = ["theme"]


class ForumAspirationAnswerResource(resources.ModelResource):
    class Meta:
        model = ForumAspirationAnswer


@admin.register(ForumAspirationAnswer)
class ForumAspirationAnswerAdmin(ExportActionModelAdmin):
    resource_class = ForumAspirationResource
    list_display = ("id", "aspiration", "group_name", "amendment", "user_id")
    list_filter = ["aspiration__theme", "aspiration"]


class PrioritizedAspirationResource(resources.ModelResource):
    class Meta:
        model = PrioritizedAspiration


@admin.register(PrioritizedAspiration)
class ForumAspirationAnswerAdmin(ExportActionModelAdmin):
    resource_class = PrioritizedAspirationResource
    list_filter = [
        "theme",
    ]
