# import the logging library
import logging

import settings

# Get the Django logger instance
logger = logging.getLogger("django")

logger.info(f"[SETTINGS] Debug mode: {settings.DEBUG}")
logger.info(f"[SETTINGS] Allow registration: {settings.ALLOW_REGISTRATION}")
logger.info(f"[SETTINGS] Allowed hosts: {settings.ALLOWED_HOSTS}")
logger.info(f"[SETTINGS] Main frontend url: {settings.FRONTEND_URL}")
logger.info(f"[SETTINGS] Allowed frontend urls: {settings.CORS_ALLOWED_ORIGINS}")
