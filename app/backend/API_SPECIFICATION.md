# REST JSON API Specification

## Authentification (/auth)

We use `djoser` with JWT tokens for authentication

[Djoser documentation](https://djoser.readthedocs.io/en/latest/getting_started.html)

### Get access and refresh tokens

/auth/jwt/create POST: Create tokens
Data: User object

Example of request:

```bash
curl --location --request POST 'localhost:8000/auth/jwt/create' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "my_username",
    "password": "my_username"
}'
```

Response:

```json
{
  "refresh": "my_refresh_token",
  "access": "my_access_token"
}
```

### Register a user

/auth/users POST: Create a new user
Headers: "Authorization: Bearer ACCESS_TOKEN"
Data: User object

```bash
curl --location --request POST 'localhost:8000/auth/users/' \
--header 'Authorization: Bearer my__admin_access_token' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "my_username",
    "password": "my_strong_password"
}'
```

### Request an authenticated route

Send the request with an authorization in headers:
"Authorization: Bearer ACCESS_TOKEN"

Example with cURL:

```bash
curl --location --request GET 'localhost:8000/api/users/' \
--header 'Authorization: Bearer my_access_token'
```

Example with the frontend axios client:

```js
import axiosClient from "@/service/axiosClient";

axiosClient({
  method: "METHOD",
  url: "/api/[ROUTE]",
  data: [DATA],
});
```

## Resources (/api)

### User Resource

/api/users GET: Retrieve the User list
Response:

```Json
[
    {
        "url": "http://localhost:8000/api/users/1/",
        "username": "user",
        "email": "user@domain.com",
        "is_staff": true
    }
]
```

/api/users POST: Add a user
/api/users/:id GET, PUT, PATCH, DELETE: Basic operations on user
User object:

```Json
{
    "username": "user",
    "email": "user@domain.com",
    "is_staff": false
}
```

### Theme resource

/api/themes GET: Retrieve the Theme list
Response:

```Json
[
    {
        "name": "theme1"
    },
    {
        "name": "theme 2"
    }
]
```

/api/themes POST: Add a theme
/api/themes/:id GET, PUT, PATCH, DELETE: Basic operations on theme
Theme object:

```Json
{
    "name": "theme1"
}
```

### Content resource

Content is a subresource of the theme.

/api/themes/:id/contents GET: Retrieve the content list for a theme
Response:

```Json
[
    {
        "id": 4,
        "title": "c1",
        "description": "desc",
        "theme": 1
    },
    {
        "id": 5,
        "title": "c3",
        "description": "tet",
        "theme": 1
    }
]
```

/api/themes/:id/content POST: Add a content
/api/themes/:id/content/:id GET, PUT, PATCH, DELETE: Basic operations on content
Content object:

```Json
{
    "id": 4,
    "title": "c1",
    "description": "desc",
    "theme": 1
}
```

### Question resource

/api/questions GET: Retrieve the Question list
Response:

```Json
[
    {
        "id": 1,
        "text": "is it ok?",
        "content": 3
    },
    {
        "id": 2,
        "text": "hello?",
        "content": 4
    }
]
```

/api/questions POST: Add a question
/api/questions/:id GET, PUT, PATCH, DELETE: Basic operations on question
Question object:

```Json
{
    "id": 1,
    "text": "is it ok?",
    "content": 3
}
```

Question can also be a subresource of Content with the following endpoints:
/api/themes/:id/content/:id/api/questions GET: Retrieve the Question list for a content
/api/themes/:id/content/:id/api/questions POST: Add a question
/api/themes/:id/content/:id/api/questions/:id GET, PUT, PATCH, DELETE: Basic operations on question

### Answer resource

Answer is a subresource of question

/api/questions/:id/answers GET: Retrieve the answer list
Response:

```Json
[
    {
        "id": 1,
        "text": "yes it is",
        "question": 1,
        "user": 123
    }
]
```

/api/questions/:id/answers POST: Add a answer
/api/questions/:id/answers/:id GET, PUT, PATCH, DELETE: Basic operations on answer
Answer object:

```Json
{
    "id": 1,
    "text": "yes it is",
    "question": 1,
    "user": 123
}
```

## Administration interface (/admin)

You can access the administration interface at: http://localhost:8000/admin/
