"""inspire URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
import environ

from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.permissions import IsAdminUser
import djoser.urls
import djoser.urls.jwt
from django.views.generic.base import RedirectView

from forum.advanced_views.aspiration import DataForumAspirationViewSet
from forum.advanced_views.aspiration_answer import (
    UserForumAspirationAnswerViewSet,
    ForumAspirationAnswerViewSet,
    DataForumAspirationAnswerViewSet,
)
from forum.advanced_views.forum import ForumDataViewSet, SimpleForumDataViewSet
from forum.views import *
from rest_framework_extensions.routers import ExtendedSimpleRouter
from django.conf import settings

# Initialise environment variables
env = environ.Env()
environ.Env.read_env()

# Routers provide an easy way of automatically determining the URL conf.
router = ExtendedSimpleRouter()

# Data routes: readonly, for admin users
# anonymized
router.register("answers/all", AnswerDataViewSet, basename="all-answers")
router.register("users/all", UserDataViewSet, basename="all-users")
router.register(
    "aspiration-answers/all",
    DataForumAspirationAnswerViewSet,
    basename="all-aspiration-answers",
)
router.register(
    "aspirations/all",
    DataForumAspirationViewSet,
    basename="all-aspirations",
)
# not anonymized
router.register("forums/emails", SimpleForumDataViewSet, basename="all-forums-simple")
router.register("forums/all", ForumDataViewSet, basename="all-forums")

# Readonly views

# Theme views
router.register("themes", ThemeViewSet, basename="theme").register(
    "contents",
    ContentViewSet,
    basename="theme-content",
    parents_query_lookups=["theme"],
).register(
    "questions",
    QuestionViewSet,
    basename="theme-content-question",
    parents_query_lookups=["content__theme", "content"],
)
# Question view
router.register("questions", QuestionViewSet).register(
    "answers",
    AnswerDataViewSet,
    basename="question-answers",
    parents_query_lookups=["question"],
)
# Answer view: read/write for a user only
router.register("answers", UserAnswerViewSet)


router.register(
    "aspirations",
    ForumAspirationViewSet,
    basename="aspirations",
)

router.register(
    "aspiration-answers/me",
    UserForumAspirationAnswerViewSet,
    basename="aspiration-answers/me",
)

router.register(
    "aspiration-answers",
    ForumAspirationAnswerViewSet,
    basename="aspiration-answers",
)

FAVICON_PATH = getattr(settings, "FAVICON_PATH", "%sfavicon.ico" % settings.STATIC_URL)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(router.urls)),
    # override djoser default jwt/create route
    re_path(
        r"^auth/jwt/create/?", CustomTokenObtainPairView.as_view(), name="jwt-create"
    ),
    # path to djoser end points
    path("auth/", include(djoser.urls)),
    path("auth/", include(djoser.urls.jwt)),
    # path to Django auth urls, including reset password urls
    path("accounts/", include("django.contrib.auth.urls")),
    # Redirect to frontend
    path("", RedirectView.as_view(url=settings.FRONTEND_URL), name="home"),
    url(
        r"^favicon\.ico$",
        RedirectView.as_view(url=FAVICON_PATH, permanent=True),
        name="favicon",
    ),
]

if env("DEBUG"):
    urlpatterns.append(path("api-auth/", include("rest_framework.urls")))
