#!/bin/bash

# Update packages
pipenv install

# Create .env
cp .env.example .env

# Start the database
docker-compose up -d

# Apply migrations
pipenv run python manage.py migrate forum
pipenv run python manage.py migrate

# Create a super user
pipenv run python manage.py createadmin
