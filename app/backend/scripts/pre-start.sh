#!/bin/bash

# Update packages
pipenv install

# Start the database
docker-compose up -d

# Migrate the database
pipenv run python manage.py migrate forum
pipenv run python manage.py migrate