from typing import Any


def can_be_casted_to_integer(value: Any) -> bool:
    try:
        int(value)
    except Exception:
        return False
    return True
