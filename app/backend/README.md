# Inspire Backend

## Installation

### Requirements

- [Python](https://www.python.org/downloads/) version 3.8 or higher.
- [`pip`](https://pip.pypa.io/en/stable/installing/) (often installed with python)
- [Docker](https://docs.docker.com/get-docker/)
- `bash` (installed by default on Linux/Mac OS. For Windows, you may install WSL2 or Git Bash.)

### Install the backend

Run the following commands:

Linux / Mac OS:

```bash
npm run install-backend
npm run backend
```

Windows:

```bash
npm run install-backend-win
npm run backend
```

OR

```bash
# Go to backend directory
cd app/backend

# Install pipenv
# Linux / Mac OS
python3 -m pip install pipenv
python3 -m pipenv install
# Windows
python -m pip install pipenv
python -m pipenv install
# Then, check that `pipenv` have been added to Windows PATH.

# Install and start Django server
pipenv run install
pipenv run start
```

NB: A super user is automatically created in the installation process.
Credentials are set in .env. In production, don't forget to change the password!

NB2: to run a python command, use `pipenv run python [command]` in order to use the correct environment.

NB3: To install a new python package, run `pipenv install [package-name]`, in order to keep track of dependency changes.

## Run

### Stack

Django + PostgreSQL database in a docker

### Start the backend

```bash
npm run backend
```

OR

```bash
cd app/backend
pipenv run start
```

Or, if the db is started and no change has been made to data models:

```bash
cd app/backend
pipenv run launch
```

### Update the DB

When a django model is updated, run this command to update the DB:

```bash
cd app/backend
pipenv run makemigrations
pipenv run migrate
```

which is equivalent to:

```bash
cd app/backend
pipenv run python manage.py makemigrations forum
pipenv run python manage.py makemigrations
pipenv run python manage.py migrate forum
pipenv run python manage.py migrate
```

### Stop the db

WARN: this will delete completely the database!

```bash
pipenv run stop_db
```

### Reset hard the db

WARN: this will delete completely the database!

```bash
pipenv run reset_hard_db
```

### Visualize the DB

The local db is accessible at: `http://localhost:5432`

You can use [pgadmin4](https://www.pgadmin.org/download/) to access it.
Credentials can be found in `app/backend/.env`

## Use the API

Specification of the API can be found [here](API_SPECIFICATION.md)
