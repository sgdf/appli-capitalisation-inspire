import re

# Use `clever logs` to get logs
# Use `clever logs > [PATH]` to save logs to a file

DATE_REGEX = r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z"
REGEXES = {
    "login_attempt": rf"{DATE_REGEX}:\s*\[LOGIN\] User .+ trying to log in$",
    "login_success": rf"{DATE_REGEX}:\s*\[LOGIN\] User #\d+ .+ got tokens$",
    "registration_success": rf"{DATE_REGEX}:\s*\[REGISTRATION\] User .+ registered$",
}
LOG_FILE = "./backend-prod.log"

with open(LOG_FILE, "r") as file:
    counts = {key: 0 for key in REGEXES}
    last_line = None
    for (i, line) in enumerate(file.readlines()):
        if i == 1:
            start_time = re.match(DATE_REGEX, line)
            print("Start time:", start_time.group() if start_time else "NOT FOUND")
        last_line = line
        for key, regex in REGEXES.items():
            if re.match(regex, line):
                counts[key] += 1
    end_time = re.match(DATE_REGEX, last_line)
    print("End time:", end_time.group() if end_time else "NOT FOUND")
    print(f"Counts: {counts}")
