# inspire-capitalisation

Projet de capitalisation et de remontée des différents avis des cadres et jeunes du mouvement dans le cadre de la réflexion autour du Plan d'Orientation des Scouts et Guides de France

## Web App
[Documentation](inspire-capitalisation-app/README.md)

### Front End
* VueJS


### Back End
* Postgres
* Django


