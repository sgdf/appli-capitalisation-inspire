# Extraction et analyse des données

## Installation

# Windows 10
  * Installer python (disponible sur le windows store), exemple python 3.8
  * ouvrir un terminal (cmd.exe) et aller dans le répertoire nlp 
  * créer l'environnement virtuel: python -m venv venv
  * activer l'environnement: venv\Script\activate

# Linux
  * ouvrir un terminal et aller dans le répertoire nlp
  * crééer l'environnement virtuel: virtualenv -p python3 venv
  * activer l'environnement: source venv/bin/activate

# Ensuite
  * mettre à jour pip: python -m pip install --upgrade pip
  * installer les paquets: pip install -r requirements.txt

# Execution
```
python dataframe_extractor.py
```


