#!/usr/bin/python
import getopt
import os
import requests
import pandas as pd
import sys

GLOBAL_CONFIG = {
    "uat": (("sgdf", "1$pirati0n!"), "https://backend-monpo2022.cleverapps.io/api"),
    "prd": (("login", "passwd"), "https://admin.monpo2022.sgdf.fr/api"),
}

THEMES_DICT = {
    1: "Ouverture, diversité et inclusion",
    2: "Nature",
    3: "Conversion écologique",
    4: "SGDF, acteur d'une Église fraternelle et universelle",
    5: "Engagement dans la communauté",
    6: "Image et notoriété",
    7: "Développement",
    8: "Pédagogie et activités ancrées dans leur époque",
}


def extract_answers(auth, url):
    jrqst = requests.get(url + "/answers/all/", auth=auth).json()
    df = pd.DataFrame(jrqst)

    if not df.empty:
        df.test = df.text.astype("string")
        df.participation_type = df.participation_type.astype("category")
        df.participation_unit = df.participation_unit.astype("category")
        df.question = df.question.astype("int")
        df.user = df.user.astype("int")
    return df


def extract_users(auth, url):
    jrqst = requests.get(url + "/users/all/", auth=auth).json()
    df = pd.DataFrame(jrqst)

    if not df.empty:
        df.age = df.age.fillna(0).astype("int")
        df.territory = df.territory.astype("category")
        df.group = df.group.astype("string")
        df.missions = df.missions.astype("category")
        df.group_typology = df.group_typology.astype("category")
        df.seniority = df.seniority.astype("category")
        for i in range(1, 9):
            df["theme_" + str(i)] = df.themes.apply(lambda l: i in l)
    return df


def users_rename_themes(df):
    df_out = df.copy()
    for t_k, t_v in THEMES_DICT.items():
        theme_name = "theme_" + str(t_k)
        df_out[t_v] = df_out[theme_name]
        del df_out[theme_name]
    return df_out


def extract_aspirations(auth, url):
    jrqst = requests.get(url + "/aspirations/all/", auth=auth).json()
    df = pd.DataFrame(jrqst)

    if not df.empty:
        df.id = df.id.astype("int")
        df.title = df.title.astype("string")
        df.description = df.description.astype("string")
        df.theme = df.theme.astype("int")
    return df


def extract_aspirations_answers(auth, url):
    jrqst = requests.get(url + "/aspiration-answers/all/", auth=auth).json()
    df = pd.DataFrame(jrqst)

    if not df.empty:
        df.id = df.id.astype("int")
        df.aspiration_id = df.aspiration_id.astype("int")
        df.pros_arguments = df.pros_arguments.astype("string")
        df.cons_arguments = df.cons_arguments.astype("string")
        df.amendment = df.amendment.astype("string")
    return df


def extract_questions(auth, url):
    jrqst = requests.get(url + "/questions/", auth=auth).json()
    df = pd.DataFrame(jrqst)

    if not df.empty:
        df.type = df.type.astype("category")
        df.text = df.text.astype("string")  # TODO: decode QCU type
        df.content = df.content.fillna(-1).astype("int")
        df.theme = df.theme.fillna(-1).astype("int")
    return df


def extract_themes(auth, url):
    jrqst = requests.get(url + "/themes/", auth=auth).json()
    df = pd.DataFrame(jrqst)

    if not df.empty:
        df.name = df.name.astype("string")
        df.description = df.description.astype("string")
        df.forum = df.forum.astype("int")
        df.date = df.date.astype("string")
        # df["date_from"] = df.date.apply(lambda d: datetime.datetime.strptime(" ".join([d.split(" ")[0], d.split(" ")[2]]), "%d/%m/%Y %Hh%M"))
        # df["date_to"] = df.date.apply(lambda d: datetime.datetime.strptime(" ".join([d.split(" ")[0], d.split(" ")[4]]), "%d/%m/%Y %Hh%M"))
        # del df["date"]
        df.contents = df.contents.astype("string")  # TODO: decode
    return df


def extract_forums(auth, url):
    jrqst = requests.get(url + "/forums/all", auth=auth).json()
    df = pd.DataFrame(jrqst)

    return df


def main(argv):
    login = os.getenv("SGDF_LOGIN")
    password = os.getenv("SGDF_PASSWORD")
    settings = "uat"
    out_path = "dist"

    try:
        opts, args = getopt.getopt(argv, "h", ["help", "login=", "password=", "prd"])
    except getopt.GetoptError as e:
        print("<prg>.py --prd --login <login> -password <password>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("<prg>.py -prd -login <login> -password <password>")
            sys.exit()
        elif opt in ("--login"):
            login = arg
        elif opt in ("--password"):
            password = arg
        elif opt in ("--prd"):
            settings = "prd"

    auth, url = GLOBAL_CONFIG[settings]
    if login is not None and password is not None:
        auth = (login, password)

    if not os.path.exists(out_path):
        os.makedirs(out_path)

    df_answers = extract_answers(auth, url)
    df_answers.to_csv(os.path.join(out_path, "answers.csv"), sep=";", index=False)

    df_users = extract_users(auth, url)
    df_users_export = users_rename_themes(df_users)
    df_users_export.to_csv(os.path.join(out_path, "users.csv"), sep=";", index=False)

    df_aspirations = extract_aspirations(auth, url)
    df_aspirations.to_csv(
        os.path.join(out_path, "aspirations.csv"), sep=";", index=False
    )

    df_extract_aspirations_answers = extract_aspirations_answers(auth, url)
    df_extract_aspirations_answers.to_csv(
        os.path.join(out_path, "aspirations_answers.csv"), sep=";", index=False
    )

    df_questions = extract_questions(auth, url)
    df_questions.to_csv(os.path.join(out_path, "questions.csv"), sep=";", index=False)

    df_themes = extract_themes(auth, url)
    df_themes.to_csv(os.path.join(out_path, "themes.csv"), sep=";", index=False)

    # df_forums = extract_forums(auth, url)
    # df_forums.to_csv(os.path.join(out_path, "forums.csv"), sep=";", index=False)


if __name__ == "__main__":
    main(sys.argv[1:])
