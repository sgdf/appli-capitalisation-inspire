#!/usr/bin/python
import getopt
import os
import matplotlib.pyplot as plt
import numpy as np
import sys


from dataframe_extractor import extract_users, users_rename_themes, global_config


def main(argv):
    login = os.getenv("SGDF_LOGIN")
    password = os.getenv("SGDF_PASSWORD")
    settings = "uat"
    out_path = "dist"

    try:
        opts, args = getopt.getopt(argv, "h", ["help", "login=", "password=", "prd"])
    except getopt.GetoptError as e:
        print("<prg>.py -prd --login <login> --password <password>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("<prg>.py -prd --login <login> --password <password>")
            sys.exit()
        elif opt in ("--login"):
            login = arg
        elif opt in ("--password"):
            password = arg
        elif opt in ("--prd"):
            settings = "prd"

    auth, url = global_config[settings]
    if login is not None and password is not None:
        auth = (login, password)

    if not os.path.exists(out_path):
        os.makedirs(out_path)

    df_users = extract_users(auth, url)

    drop_index = df_users[df_users.is_staff == True].index
    df_users.drop(drop_index, inplace=True)

    total = len(df_users)

    # Territoires
    territories = df_users.groupby(["territory"]).size()
    plt.clf()
    fig, ax = plt.subplots()
    fig.set_figheight(10)
    fig.set_figwidth(20)
    ax.bar(x=range(len(territories)), height=territories.values)
    ax.set_ylabel("Nombre de participants")
    ax.set_xticks(np.arange(len(territories.index)))
    ax.set_xticklabels(territories.index, rotation=45, ha="right")
    ax.set_title(f"Participants par territoire (Total: {total})")
    fig.tight_layout()
    fig.savefig(os.path.join(out_path, "territoires.png"))

    # missions
    missions = df_users.groupby(["missions"]).size()
    plt.clf()
    fig, ax = plt.subplots()
    fig.set_figheight(10)
    fig.set_figwidth(20)
    ax.bar(x=range(len(missions)), height=missions.values)
    ax.set_ylabel("Nombre de participants")
    ax.set_xticks(np.arange(len(missions.index)))
    ax.set_xticklabels(missions.index, rotation=45, ha="right")
    ax.set_title(f"Participants par mission (Total: {total})")
    fig.tight_layout()
    fig.savefig(os.path.join(out_path, "missions.png"))

    # Themes par utilisateur
    col_names = [col for col in df_users.columns if col.startswith("theme_")]
    df_users_theme = df_users[col_names]
    df_users_theme = users_rename_themes(df_users_theme)
    df_users_theme.sort_index(axis=1, inplace=True)
    counts = [sum(df_users_theme[t]) for t in df_users_theme.columns]
    plt.clf()
    fig, ax = plt.subplots()
    fig.set_figheight(10)
    fig.set_figwidth(20)
    ax.bar(x=range(len(counts)), height=counts)
    ax.set_ylabel("Nombre de participants")
    ax.set_xticks(np.arange(len(df_users_theme.columns)))
    ax.set_xticklabels(df_users_theme.columns, rotation=45, ha="right")
    ax.set_title(f"Themes par participants (Total: {total})")
    fig.tight_layout()
    fig.savefig(os.path.join(out_path, "users_themes.png"))


if __name__ == "__main__":
    main(sys.argv[1:])
